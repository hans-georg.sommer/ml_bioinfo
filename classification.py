#!/usr/bin/env python3

import numpy as np

def perceptron_train(x_train_mat, y_train_vec, eta_par = 0.1, max_runs = 500):
    # init
    weights_vec = np.zeros(x_train_mat[0].size)
    bias_par = 0
    n_updates_train = 0
    R = np.amax([np.linalg.norm(v) for v in x_train_mat])

    # training
    for r in range(max_runs):
        updated = False
        for i in range(y_train_vec.size):
            data_vec = x_train_mat[i]
            label = y_train_vec[i]
            if label * (np.dot(weights_vec, data_vec) + bias_par) <= 0:
                weights_vec += eta_par * label * data_vec
                bias_par += eta_par * label * R
                n_updates_train += 1
                updated = True
        if not updated:
            break
    return weights_vec, bias_par, n_updates_train

def perceptron_test(x_test_mat, y_test_vec, weights_vec, bias_par):
    test_result_vec = y_test_vec * (np.dot(x_test_mat, weights_vec) + bias_par)
    return test_result_vec[test_result_vec <= 0].size


def centroid_train(x_train_mat, y_train_vec):
    centroid_pos_vec = np.mean(x_train_mat[y_train_vec == 1], axis=0)
    centroid_neg_vec = np.mean(x_train_mat[y_train_vec == -1], axis=0)
    return centroid_pos_vec, centroid_neg_vec

def centroid_test(x_test_mat, y_test_vec, centroid_pos_vec, centroid_neg_vec):
    diff_pos_mat = np.linalg.norm(x_test_mat - centroid_pos_vec, axis=1)
    diff_neg_mat = np.linalg.norm(x_test_mat - centroid_neg_vec, axis=1)
    result_vec = np.where(diff_pos_mat < diff_neg_mat, 1, -1) - y_test_vec
    return result_vec[result_vec != 0].size
