#!/usr/bin/env python3

import numpy as np

def generate_data(n_points, n_dims, offset, shuffle = True):
    '''
    Function to generate a random dataset with n_points labeled positive
    and n_points labeled negative. The negative points are shifted by offset.
    If shuffle is True, the data and labels are shuffled.
    '''
    n = n_points // 2
    x_pos_mat = np.random.rand(n, n_dims)
    x_neg_mat = np.random.rand(n, n_dims) + offset
    x_train_mat = np.vstack((x_pos_mat, x_neg_mat))
    y_train_vec = np.hstack((np.ones(n), np.full(n, -1)))
    if shuffle:
        perm = np.random.permutation(n * 2)
        x_train_mat = x_train_mat[perm]
        y_train_vec = y_train_vec[perm]
    return x_train_mat, y_train_vec

def read_sequence_data(positive_seq_file, negative_seq_file):
    seq_chars = ['A', 'C', 'G', 'T']

    with open(positive_seq_file) as f:
        pos_sequences = f.readlines()
    with open(negative_seq_file) as f:
        neg_sequences = f.readlines()

    # assumes, that all sequences have the same length
    pos_n = len(pos_sequences)
    neg_n = len(neg_sequences)
    seq_len = len(pos_sequences[0]) - 1

    x_pos_mat = np.empty((pos_n, seq_len * len(seq_chars)))
    x_neg_mat = np.empty((neg_n, seq_len * len(seq_chars)))
    label_vec = np.hstack((np.ones(pos_n), np.full(neg_n, -1)))

    # generate binary vectors for each sequence
    for i in range(pos_n):
        x_pos_mat[i] = np.hstack(np.array(tuple(pos_sequences[i][:-1])) == char for char in seq_chars)
    for i in range(neg_n):
        x_neg_mat[i] = np.hstack(np.array(tuple(neg_sequences[i][:-1])) == char for char in seq_chars)

    return np.vstack((x_pos_mat, x_neg_mat)), label_vec

def randomize_data(x_mat, label_vec):
    rand_index_vec = np.random.permutation(label_vec.size)

    # has to use np.array_split() instead of np.split() here
    # to account for uneven number of sequences
    x_train_mat, x_test_mat = np.array_split(x_mat[rand_index_vec], 2)
    y_train_vec, y_test_vec = np.array_split(label_vec[rand_index_vec], 2)
    return x_train_mat, x_test_mat, y_train_vec, y_test_vec
